<?php

require_once("model/GameDB.php");
require_once("ViewHelper.php");

class GameController {

    public static function index() {
        if (isset($_GET["id"])) {
            ViewHelper::render("view/game-detail.php", ["game" => GameDB::get($_GET["id"]), 
                "score" => GameDB::getScore($_GET["id"])]);
        } else {
            ViewHelper::render("view/game-list.php", ["games" => GameDB::getAll()]);
        }
    }

    public static function search() {
        if (isset($_GET["query"])) {
            $query = $_GET["query"];
            $hits = GameDB::search($query);
        } else {
            $query = "";
            $hits = [];
        }

        ViewHelper::render("view/game-search.php", ["hits" => $hits, "query" => $query]);
    }
}