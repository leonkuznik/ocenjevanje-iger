<?php

require_once("model/GameDB.php");
require_once("ViewHelper.php");

class RankingController {

    public static function index() {
        $vars = [
            "games" => GameDB::getAllDescScore()
        ];

        ViewHelper::render("view/ranking-index.php", $vars);
    }

    public static function rankGame() {
        $idGame = isset($_POST["id"]) ? intval($_POST["id"]) : null;
        $score = isset($_POST["rank"]) ? intval($_POST["rank"]) : null;
        $idUser = UserDB::getUserId($_SESSION["user"]);

        if (GameDB::getScoreByUser($idGame, $_SESSION["user"]) == 0) {
            GameDB::insertScore($idGame, $idUser, $score);
        }
        else {
            GameDB::updateScore($idGame, $idUser, $score);
        }

        ViewHelper::redirect(BASE_URL . "ranking");
    }
}