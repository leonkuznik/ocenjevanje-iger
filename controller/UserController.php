<?php

require_once("model/UserDB.php");
require_once("ViewHelper.php");

class UserController {

    public static function showLoginForm() {
       ViewHelper::render("view/user-login-form.php");
    }

    public static function logout() {
        UserDB::logout();
        ViewHelper::redirect(BASE_URL . "game");
    }

    public static function showRegisterForm($data = [], $errors = []) {
        // If $data is an empty array, let's set some default values
        if (empty($data)) {
            $data = [
                "username" => "",
                "password" => ""
            ];
        }

        // If $errors array is empty, let's make it contain the same keys as
        // $data array, but with empty values
        if (empty($errors)) {
            foreach ($data as $key => $value) {
                $errors[$key] = "";
            }
        }

        $vars = ["user" => $data, "errors" => $errors];
        ViewHelper::render("view/user-register-form.php", $vars);
    }

    public static function register() {
        $rules = [
            "username" => [
                // Only letters, dots, spaces and dashes are allowed
                "filter" => FILTER_VALIDATE_REGEXP,
                "options" => ["regexp" => "/^[^\W_]+$/"]
            ],
            // we convert HTML special characters
            "password" => [
                "filter" => FILTER_VALIDATE_REGEXP,
                "options" => ["regexp" => "/^[a-zA-Z0-9]+$/"]
            ]
        ];
        // Apply filter to all POST variables; from here onwards we never
        // access $_POST directly, but use the $data array
        $data = filter_input_array(INPUT_POST, $rules);

        $errors["username"] = empty($data["username"]) ? "Provide the username: 
            only letters and numbers allowed." : "";
        $errors["password"] = empty($data["password"]) ? "Provide the password: 
            only letters and numbers allowed." : "";
        if (UserDB::validSignupUsername($data["username"])) {
            $errors["errorMessage"] = "";
        }
        else {
            $errors["errorMessage"] = "Username already exists.";
        }

        // Is there an error?
        $isDataValid = true;
        foreach ($errors as $error) {
            $isDataValid = $isDataValid && empty($error);
        }

        if ($isDataValid) {
            if (UserDB::validSignupUsername($data["username"])) {
                UserDB::insert($data["username"], $data["password"]);
                ViewHelper::redirect(BASE_URL . "user/login");
            }
            else {
                self::showRegisterForm($data, $errors);
            }
        } else {
            self::showRegisterForm($data, $errors);
        }
    }

    public static function login() {
       if (UserDB::validLoginAttempt($_POST["username"], $_POST["password"])) {
            /* $vars = [
                "username" => $_POST["username"],
                "password" => $_POST["password"]
            ]; */

            ViewHelper::redirect(BASE_URL . "game");
            /* ViewHelper::render("view/user-secret-page.php", $vars); */
       } else {
            ViewHelper::render("view/user-login-form.php", 
                ["errorMessage" => "Invalid username or password."]);
       }
    }
}