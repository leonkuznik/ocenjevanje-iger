<?php

require_once "DBInit.php";

class UserDB {

    // Returns true if a valid combination of a username and a password are provided.
    public static function validLoginAttempt($username, $password) {
        $dbh = DBInit::getInstance();

        $query = "SELECT COUNT(id) FROM user WHERE username = :username AND password = :password";
        $stmt = $dbh->prepare($query);
        $stmt->bindParam(":username", $username);
        $stmt->bindParam(":password", $password);
        $stmt->execute();

        if ($stmt->fetchColumn(0) == 1) {
            $_SESSION["user"] = $username;
            return true;
        }
        
        return false;
    }

    public static function validSignupUsername($username) {
        $dbh = DBInit::getInstance();

        $query = "SELECT COUNT(id) FROM user WHERE username = :username";
        $stmt = $dbh->prepare($query);
        $stmt->bindParam(":username", $username);
        $stmt->execute();

        return !$stmt->fetchColumn(0) == 1;
    }

    public static function insert($username, $password) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("INSERT INTO user (username, password) VALUES (:username, :password)");
        $statement->bindParam(":username", $username);
        $statement->bindParam(":password", $password);
        $statement->execute();
    }

    public static function logout() {
        unset($_SESSION["user"]);
    }

    public static function getUserId($username) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT id FROM user WHERE username = :username");
        $statement->bindParam(":username", $username);
        $statement->execute();

        $userId = $statement->fetch();

        if ($userId != null) {
            return $userId["id"];
        } else {
            throw new InvalidArgumentException("No record with username $username");
        }
    }

    public static function getUser($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT username FROM user WHERE id = :id");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();

        $user = $statement->fetch();

        if ($user != null) {
            return $user;
        } else {
            throw new InvalidArgumentException("No record with id $id");
        }
    }
}
