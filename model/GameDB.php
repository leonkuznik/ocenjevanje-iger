<?php

require_once "DBInit.php";

class GameDB {

    public static function getForIds($ids) {
        $db = DBInit::getInstance();

        $id_placeholders = implode(",", array_fill(0, count($ids), "?"));

        $statement = $db->prepare("SELECT id, title, description, year FROM game 
            WHERE id IN (" . $id_placeholders . ")");
        $statement->execute($ids);

        return $statement->fetchAll();
    }

    public static function getAllDescScore() {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT g.id, g.title, g.description, g.year, ROUND(AVG(gs.score), 1) as score 
            FROM game g, gamescore gs WHERE g.id = gs.idGame GROUP BY g.id ORDER BY score DESC");
        $statement->execute();

        return $statement->fetchAll();
    }

    public static function getAll() {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT id, title, description, year FROM game");
        $statement->execute();

        return $statement->fetchAll();
    }

    public static function getAllScores() {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT ROUND(AVG(score), 1) as score FROM gamescore GROUP BY idGame");
        $statement->execute();

        return $statement->fetchAll();
    }

    public static function get($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT id, title, description, year FROM game WHERE id = :id");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();

        $game = $statement->fetch();

        if ($game != null) {
            return $game;
        } else {
            throw new InvalidArgumentException("No record with id $id");
        }
    }

    public static function getScore($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT ROUND(AVG(score), 1) AS score FROM gamescore 
            WHERE idGame = :idGame GROUP BY idGame");
        $statement->bindParam(":idGame", $id, PDO::PARAM_INT);
        $statement->execute();

        $score = $statement->fetch();

        if ($score != null) {
            return $score["score"];
        } else {
            return 0;
        }
    }

    public static function getScoreByUser($idGame, $username) {
        $db = DBInit::getInstance();
        $idUser = UserDB::getUserId($username);
        //$idUser = 2;

        $statement = $db->prepare("SELECT score FROM gamescore WHERE idGame = :idGame AND idUser = :idUser");
        $statement->bindParam(":idGame", $idGame, PDO::PARAM_INT);
        $statement->bindParam(":idUser", $idUser, PDO::PARAM_INT);
        $statement->execute();

        $score = $statement->fetch();

        if ($score != null) {
            return $score["score"];
        } else {
            return 0;
        }
    }

    public static function insertScore($idGame, $idUser, $score) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("INSERT INTO gamescore (idGame, idUser, score) 
            VALUES (:idGame, :idUser, :score)");
        $statement->bindParam(":idGame", $idGame, PDO::PARAM_INT);
        $statement->bindParam(":idUser", $idUser, PDO::PARAM_INT);
        $statement->bindParam(":score", $score);
        $statement->execute();
    }

    public static function updateScore($idGame, $idUser, $score) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("UPDATE gamescore SET score = :score WHERE idGame = :idGame 
            AND idUser = :idUser");
        $statement->bindParam(":idGame", $idGame, PDO::PARAM_INT);
        $statement->bindParam(":idUser", $idUser, PDO::PARAM_INT);
        $statement->bindParam(":score", $score);
        $statement->execute();
    }    

    public static function search($query) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT id, title, description, year FROM game 
            WHERE MATCH (title, description) AGAINST (:query IN BOOLEAN MODE)");
        $statement->bindValue(":query", $query);
        $statement->execute();

        return $statement->fetchAll();
    }    
}
