<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Game detail</title>

<header class="col-12">
    <h1>Domača naloga 3: Spletna stran za ocenjevanje videoiger</h1>
</header>

<div class="row">
    <nav class="col-3 col-s-12">
        <?php include("view/user-loggedin.php"); ?>
        <?php include("view/menu-links.php"); ?>
    </nav>

    <article class="col-9 col-s-12">
        <h2>Details of: <?= $game["title"] ?></h2>

        <ul>
            <li>Title: <b><?= $game["title"] ?></b></li>
            <li>Release year: <b><?= $game["year"] ?></b></li>
            <li>Score: <b><?= $score ?></b></li>
        </ul>

        <?php if ($game["description"] != null): ?>
            <p><?= $game["description"] ?></p>
        <?php endif; ?>
    </article>
</div>

<footer class="col-12">
    <p>Some footer text.</p>
</footer>
