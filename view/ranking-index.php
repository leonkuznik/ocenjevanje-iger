<!DOCTYPE html>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />

<title>Game ranking</title>

<header class="col-12">
    <h1>Domača naloga 3: Spletna stran za ocenjevanje videoiger</h1>
</header>

<div class="row">
    <nav class="col-3 col-s-12">
        <?php include("view/user-loggedin.php"); ?>
        <?php include("view/menu-links.php"); ?>
    </nav>

    <article class="col-9 col-s-12">
        <h2>Game ranking</h2>
        <?php foreach ($games as $game): ?>

            <div class="game">
                <form action="<?= BASE_URL . "ranking/rank-game" ?>" method="post" />
                    <input type="hidden" name="id" value="<?= $game["id"] ?>" />
                    <p><a href="<?= BASE_URL . "game?id=" . $game["id"] ?>"><?= $game["title"] ?></a></p>
                    <p>Score: <?= GameDB::getScore($game["id"]) ?> </p>
                    <?php if (isset($_SESSION["user"])): ?>
                        <p>
                            <input type="radio" name="rank" value="1" required 
                                <?php if (GameDB::getScoreByUser($game["id"], $_SESSION["user"]) == 1): ?> 
                                    checked 
                                <?php endif; ?>> 1

                            <input type="radio" name="rank" value="2" required
                                <?php if (GameDB::getScoreByUser($game["id"], $_SESSION["user"]) == 2): ?> 
                                    checked 
                                <?php endif; ?>> 2

                            <input type="radio" name="rank" value="3" required
                                <?php if (GameDB::getScoreByUser($game["id"], $_SESSION["user"]) == 3): ?> 
                                    checked 
                                <?php endif; ?>> 3

                            <input type="radio" name="rank" value="4" required
                                <?php if (GameDB::getScoreByUser($game["id"], $_SESSION["user"]) == 4): ?> 
                                    checked 
                                <?php endif; ?>> 4

                            <input type="radio" name="rank" value="5" required
                                <?php if (GameDB::getScoreByUser($game["id"], $_SESSION["user"]) == 5): ?> 
                                    checked 
                                <?php endif; ?>> 5
                        </p>
                        <p><button>Rank</button></p>
                    <?php endif; ?>
                </form> 
            </div>

        <?php endforeach; ?>
    </article>
</div>

<footer class="col-12">
    <p>Some footer text.</p>
</footer>
