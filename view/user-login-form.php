<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Login form</title>

<header class="col-12">
    <h1>Domača naloga 3: Spletna stran za ocenjevanje videoiger</h1>
</header>

<div class="row">
    <nav class="col-3 col-s-12">
        <?php include("view/menu-links.php"); ?>
    </nav>

    <article class="col-9 col-s-12">
        <h2>Please log in</h2>

        <?php if (!empty($errorMessage)): ?>
            <p class="important"><?= $errorMessage ?></p>
        <?php endif; ?>

        <form action="<?= BASE_URL . "user/login" ?>" method="post">
            <p>
                <label>Username: <input type="text" name="username" autocomplete="off" 
                    required autofocus /></label>
            </p>
            <p>
                <label>Password: <input type="password" name="password" required /></label>
            </p>
            <p class="loginsignup"><button>Log-in</button></p>
            <p>Don't have an account yet? <a href="<?= BASE_URL . "user/register" ?>">Sign up</a></p>
        </form>
    </article>
</div>

<footer class="col-12">
    <p>Some footer text.</p>
</footer>