<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Sign up</title>

<header class="col-12">
    <h1>Domača naloga 3: Spletna stran za ocenjevanje videoiger</h1>
</header>

<div class="row">
    <nav class="col-3 col-s-12">
        <?php include("view/menu-links.php"); ?>
    </nav>

    <article class="col-9 col-s-12">
        <h2>Sign up</h2>

        <?php if (!empty($errors["errorMessage"])): ?>
            <p class="important"><?= $errors["errorMessage"] ?></p>
        <?php endif; ?>

        <form action="<?= BASE_URL . "user/register" ?>" method="post">
            <p><label>Username: <input type="text" name="username" value="<?= $user["username"] ?>" required autofocus />
                <span class="important"><?= $errors["username"] ?></span>
            </label></p>
            <p><label>Password: <input type="password" name="password" value="<?= $user["password"] ?>" />
                <span class="important"><?= $errors["password"] ?></span></label>
            </p>
            <p class="loginsignup"><button>Sign up</button></p>
        </form>
    </article>
</div>

<footer class="col-12">
    <p>Some footer text.</p>
</footer>
