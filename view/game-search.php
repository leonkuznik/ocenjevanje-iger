<!DOCTYPE html>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />

<title>Game search</title>

<header class="col-12">
    <h1>Domača naloga 3: Spletna stran za ocenjevanje videoiger</h1>
</header>

<div class="row">
    <nav class="col-3 col-s-12">
        <?php include("view/user-loggedin.php"); ?>
        <?php include("view/menu-links.php"); ?>
    </nav>

    <article class="col-9 col-s-12">
        <h2>Game search</h2>

        <form action="<?= BASE_URL . "game/search" ?>" method="get">
            <p>
                <label for="query">Search games:</label>
                <input type="text" name="query" id="query" value="<?= $query ?>" autofocus />
            <button>Search</button></p>
        </form>

        <ul>
            <?php foreach ($hits as $game): ?>
                <li><a href="<?= BASE_URL . "game?id=" . $game["id"] ?>"><?= $game["title"] ?> 
                    (<?= $game["year"] ?>)</a></li>
            <?php endforeach; ?>

        </ul>
    </article>
</div>

<footer class="col-12">
    <p>Some footer text.</p>
</footer>
