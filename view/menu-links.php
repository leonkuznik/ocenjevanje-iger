<a href="<?= BASE_URL . "game" ?>">All games</a>
<a href="<?= BASE_URL . "game/search" ?>">Search</a>
<a href="<?= BASE_URL . "ranking" ?>">Game ranking</a>
<?php if (isset($_SESSION["user"])): ?>
    <a href="<?= BASE_URL . "user/logout" ?>">Log-out</a>
<?php else: ?>
    <a href="<?= BASE_URL . "user/login" ?>">Log-in</a>
<?php endif; ?>
