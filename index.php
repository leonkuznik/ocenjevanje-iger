<?php

session_start();

require_once("controller/GameController.php");
require_once("controller/RankingController.php");
require_once("controller/UserController.php");

define("BASE_URL", $_SERVER["SCRIPT_NAME"] . "/");
define("CSS_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/css/");

$path = isset($_SERVER["PATH_INFO"]) ? trim($_SERVER["PATH_INFO"], "/") : "";

$urls = [
    "game" => function () {
       GameController::index();
    },
    "game/search" => function () {
        GameController::search();
    },
    "ranking" => function () {
        RankingController::index();
    },
    "ranking/rank-game" => function () {
        RankingController::rankGame();
    },
    "user/login" => function () {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            UserController::login();
        } else {
            UserController::showLoginForm();
        }
    },
    "user/register" => function () {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            UserController::register();
        } else {
            UserController::showRegisterForm();
        }
    },
    "user/logout" => function () {
        UserController::logout();
    },
    "" => function () {
        ViewHelper::redirect(BASE_URL . "game");
    },
];

try {
    if (isset($urls[$path])) {
       $urls[$path]();
    } else {
        /* echo "No controller for '$path'"; */
        ViewHelper::error404();
    }
} catch (Exception $e) {
    /* echo "An error occurred: <pre>$e</pre>"; */
    ViewHelper::error404();
} 
